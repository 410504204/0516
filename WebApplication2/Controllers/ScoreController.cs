﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication2.Controllers
{
    public class ScoreController : Controller
    {
        // GET: Score
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(int score)
        {
            string level = "";
            float Score = score;

            if (Score>=0 && Score<=20)
            {
                level= "E";
            }
            else if (Score>=21 && Score<=39)
            {
                level = "D";
            }
            else if (Score >=40 && Score <= 59)
            {
                level = "C";
            }
            else if (Score >= 60 && Score <= 79)
            {
                level = "B";
            }
            else if (Score >= 80 && Score <= 100)
            {
                level = "A";
            }
            ViewBag.Score = Score;
            ViewBag.level = level;
            return View();
        }
    }
}